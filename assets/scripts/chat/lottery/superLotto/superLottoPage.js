/*
大乐透
*/
cc.Class({
    extends: cc.Component,

    properties: {
        //投注内容节点
        ndBetPanle:{
            default: null,
            type: cc.Node
        },

        //标头
        labTopTitle:{
            default: null,
            type: cc.Label
        },
        
        //显示的投注信息
        labSelectShow:{
            default: null,
            type: cc.Label
        },

        //倍数编辑
        edMutipleNum:{
            default:null,
            type:cc.EditBox
        },

        //机选清空切换按钮
        tgSelect:{
            default:null,
            type:cc.Toggle
        },

        //投注内容
        betContent:{
            default: null,
            type: cc.Layout 
        },
        //红球
        tgRedBall:{
            default: null,
            type: cc.Prefab
        },
        //蓝球
        tgBlueBall:{
            default: null,
            type: cc.Prefab
        },

        //设胆球
        tgChangeBall:{
            default: null,
            type: cc.Node
        },

        //红球图片
        spRedBall:{
            default: null,
            type: cc.SpriteFrame
        },

        //黄球图片
        spYellowBall:{
            default: null,
            type: cc.SpriteFrame
        },

        //红球放大
        spBigRedBall:{
            default: null,
            type: cc.Sprite
        },
        //蓝球放大
        spBigBlueBall:{
            default: null,
            type: cc.Sprite
        },

        //黄球放大
        spBigYellowBall:{
            default: null,
            type: cc.Sprite
        },

        //分割线
        labTip:{
            default: null,
            type: cc.Prefab
        },

        //投注单
        betcartPage:{
            default: null,
            type: cc.Prefab
        },

        //遗漏改变----
        ndCentent:{
            default:null,
            type:cc.Node
        },
        ndBetBg:{
            default:null,
            type:cc.Node
        },
        ndBallCentent:{
            default:null,
            type:cc.Node
        },
        ndBallCentent1:{
            default:null,
            type:cc.Node
        },
        labTime:{
            default:null,
            type:cc.Label
        },
        ndAddAward:{
            default:null,
            type:cc.Node
        },

        //----------
        _isMiss:false,//是否开启遗漏
        _isAdd:false,//是否追加

        _oneBallGroups:[],//第一组球
        _twoBallGroups:[],//第二组球
        _allBalls:[],//所有球
        _selRedBall:[],
        _selBlueBall:[],
        _selYellowBall:[],
        _toggleLists:[],

        _rules:"",//规则
        _isNorm:1,//是否胆拖选择 0胆拖1标准
        _curMoney:0,
        _curBetNum:0,
        _lotteryID:0,//彩种id
        _redNums:0,
        _blueNums:0,
        _offsetY:460,//偏移y
        BASEMONEY:2,

        _changeBall:null,//切换控制球
        _betCart:null,//投注单指针
        _betManage:null//通用方法
    },

    initConfig:function(){
        this._rules = [DEFINE.LOTTERYRULESUPERBIG.STAND,DEFINE.LOTTERYRULESUPERBIG.ADDTO];
        this._redNums = 35;
        this._blueNums = 12;
        this.isNorm = 1;
        this._curMoney=0;
        this._curBetNum=0;
        this.BASEMONEY = this._isAdd==true ? 3:2;
        this._offsetY = 460;
    },

        //外部传参
    init:function(lotteryid){
        this._lotteryID = lotteryid;  
        this.labTopTitle.string = LotteryUtils.getLotteryTypeDecByLotteryId(this._lotteryID);
        this.initConfig();
    },

    initReset:function(){
       this.onCreateCart();
    },

    //自动选择号码
    autoSelNums:function(multiple,isNorm,isAdd,numsArrys){
       // cc.log("autoSelNums");
        this.setMutipleAmount(multiple);
        this.node.getChildByName("toggle").getComponent(cc.Toggle).isChecked = isAdd;
        this._isAdd = isAdd;
        this.BASEMONEY = this._isAdd ==true ? 3:2;
        
        if(isNorm==0)//胆拖
        {
            var numsArry0 = numsArrys[0].split("#");
            var numsArry3 = numsArrys[1].split(",");
            try {
                var numsArry1 = numsArry0[0].split(",");
                var numsArry2 = numsArry0[1].split(",");
                this.isNorm = 0;
                for(var i=0;i<numsArry1.length;i++)
                {
                    var item = this._oneBallGroups[numsArry1[i]-1];
                    item.getComponent(cc.Toggle).isChecked = true;
                    var temp = item.getComponent(cc.Toggle).checkEvents[0].customEventData;
                    this.onSelRedBallCallback(item.getComponent(cc.Toggle),temp);
                }
                this.isNorm = 1;
                for(var i=0;i<numsArry2.length;i++)
                {
                    var item = this._oneBallGroups[numsArry2[i]-1];
                    item.getComponent(cc.Toggle).isChecked = true;
                    var temp = item.getComponent(cc.Toggle).checkEvents[0].customEventData;
                    this.onSelRedBallCallback(item.getComponent(cc.Toggle),temp);
                }
                for(var i=0;i<numsArry3.length;i++)
                {
                    var item = this._twoBallGroups[numsArry3[i]-1];
                    item.getComponent(cc.Toggle).isChecked = true;
                    var temp = item.getComponent(cc.Toggle).checkEvents[0].customEventData;
                    this.onSelBlueBallCallback(item.getComponent(cc.Toggle),temp);
                }
    
            } catch (error) {
                cc.log("erro superlottopage autoselnums:"+error);
            }
        }
        else
        {
            try {
                this.isNorm = 1;
                var numsArry1 = numsArrys[0].split(",");
                var numsArry2 = numsArrys[1].split(",");
                for(var i=0;i<numsArry1.length;i++)
                {
                    var item = this._oneBallGroups[numsArry1[i]-1];
                    item.getComponent(cc.Toggle).isChecked = true;
                    var temp = item.getComponent(cc.Toggle).checkEvents[0].customEventData;
                    this.onSelRedBallCallback(item.getComponent(cc.Toggle),temp);
                }
                for(var i=0;i<numsArry2.length;i++)
                {
                    var item = this._twoBallGroups[numsArry2[i]-1];
                    item.getComponent(cc.Toggle).isChecked = true;
                    var temp = item.getComponent(cc.Toggle).checkEvents[0].customEventData;
                    this.onSelBlueBallCallback(item.getComponent(cc.Toggle),temp);
                }
    
            } catch (error) {
                cc.log("erro superlottopage autoselnums:"+error);
            }
        }
    },

    //退出界面时重置界面
    clearAllBetRecord:function(){
       this.clearCurBetRecord();

        if(this._betCart)
        {
             this._betCart.destroy();
             this._betCart = null;
        }
    },

    // use this for initialization
    onLoad: function () {
        this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
                console.log('superLottoPage node  TOUCH_END');
                var contentRect = this.ndBetPanle.getBoundingBoxToWorld();
                var touchLocation = event.getLocation();
                contentRect.x = contentRect.x -20;
                contentRect.width = 1080;
                if(cc.rectContainsPoint(contentRect, touchLocation) == false){//关闭投注界面
                   // cc.log("bet betPanel close");
                    this.onClose();
                }
        }, this);
        this.initPanel();
        this._toggleLists.push(this._oneBallGroups);
        this._toggleLists.push(this._twoBallGroups);
        this._betManage = cc.find("Canvas").getChildByName("ChatRoomPage").getChildByName("BetManage").getComponent("BetManage");
    },

    showAddAward:function(data){
        for(var i=0;i<data.length;i++)
        {
            var codestr = data[i].PlayCode.toString();
            var code = codestr.substring(codestr.length-2,codestr.length);
            var index = this._rules.indexOf(code); 
            if(index != -1)
            {
                this.ndAddAward.active = true;
            }
        }
    },

        //创建号码栏
    onCreateCart:function(){
        if(this._betCart == null)
        {
            var canvas = cc.find("Canvas");
            this._betCart = cc.instantiate(this.betcartPage);
            this._betCart.getComponent("BetCartPage").init(this.node.getComponent("superLottoPage"));
            canvas.addChild(this._betCart);
            this._betCart.active = false;
        }
    },

    //是否追加
    onAddToggle:function(toggle){
        this._isAdd = toggle.getComponent(cc.Toggle).isChecked;
        this.BASEMONEY = this._isAdd ==true ? 3:2;
        this.setShowAmount(this.getMutipleAmount(),this.getBetNum());
    },

    initPanel:function(){
        //红球
        for(var i = 0;i<this._redNums;i++)
        {      
            var ball = cc.instantiate(this.tgRedBall);
            var tempNum = i+1;
            var oneNum = tempNum>9 ? tempNum.toString():"0"+tempNum.toString(); 
            ball.getComponent('Bet_toggle_ball').init({
                num: oneNum,
                miss: (this._isMiss == true)?i:""
            });
            var checkEventHandler = new cc.Component.EventHandler();
            checkEventHandler.target = this.node; 
            checkEventHandler.component = "superLottoPage"
            checkEventHandler.handler = "onSelRedBallCallback";
            checkEventHandler.customEventData = {index:i,num:oneNum};//index 球下标 num 球数值 type:球类型 0白球1红2蓝3黄球
            ball.getComponent(cc.Toggle).checkEvents.push(checkEventHandler);
            this.betContent.node.addChild(ball);
            this._oneBallGroups.push(ball);
            this._allBalls.push(ball);

                 //事件监听
            //当手指在目标节点区域内离开屏幕时。
            ball.on(cc.Node.EventType.TOUCH_END, function (event) {
                if(this.isNorm == 0)
                {
                    this.spBigYellowBall.node.y = 10000;
                }
                else
                {
                    this.spBigRedBall.node.y = 10000;
                }
            },this);

            //当手指在目标节点区域外离开屏幕时。
            ball.on(cc.Node.EventType.TOUCH_CANCEL, function (event) {
                if(this.isNorm == 0)
                {
                    this.spBigYellowBall.node.y = 10000;
                }
                else
                {
                    this.spBigRedBall.node.y = 10000;
                }
            },this);

            //当手指触摸到屏幕时。
            ball.on(cc.Node.EventType.TOUCH_START, function (event) {
                var curTg = event.getCurrentTarget();
                var pos = curTg.getPosition();
                if(this.isNorm == 0)
                {
                    this.spBigYellowBall.node.getChildByName("labnum").getComponent(cc.Label).string = curTg.getComponent('Bet_toggle_ball').getNum();
                    this.spBigYellowBall.node.x = pos.x;
                    this.spBigYellowBall.node.y = pos.y-this._offsetY;
                }
                else
                {
                    this.spBigRedBall.node.getChildByName("labnum").getComponent(cc.Label).string = curTg.getComponent('Bet_toggle_ball').getNum();
                    this.spBigRedBall.node.x = pos.x;
                    this.spBigRedBall.node.y = pos.y-this._offsetY;
                }
            },this);

            //当鼠标在目标节点在目标节点区域中移动时，不论是否按下。
            ball.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                var curTg = event.getCurrentTarget();
                var contentRect = curTg.getBoundingBoxToWorld();
                var touchLocation = event.getLocation();
                if(cc.rectContainsPoint(contentRect, touchLocation) == false){
                    if(this.isNorm == 0)
                    {
                        this.spBigYellowBall.node.y = 10000;
                    }
                    else
                    {
                        this.spBigRedBall.node.y = 10000;
                    }
                }  
            },this);

            //当手指在屏幕上目标节点区域内移动时。
            ball.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
                var curTg = event.getCurrentTarget();
                var contentRect = curTg.getBoundingBoxToWorld();
                var touchLocation = event.getLocation();
                if(cc.rectContainsPoint(contentRect, touchLocation) == false){
                    if(this.isNorm == 0)
                    {
                        this.spBigYellowBall.node.y = 10000;
                    }
                    else
                    {
                        this.spBigRedBall.node.y = 10000;
                    }
                }  
            },this);
        }

        //切换球
        this._changeBall = cc.instantiate(this.tgChangeBall);
        var checkEventHandler = new cc.Component.EventHandler();
        checkEventHandler.target = this.node; 
        checkEventHandler.component = "superLottoPage"
        checkEventHandler.handler = "onChangeBallCallback";
        checkEventHandler.customEventData = "";
        this._changeBall.getComponent(cc.Toggle).checkEvents.push(checkEventHandler);
        this.betContent.node.addChild(this._changeBall);
        this._changeBall.active = true;

        //分割线
        var line = cc.instantiate(this.labTip);
        this.betContent.node.addChild(line);

        //蓝球
        for(var i = 0;i<this._blueNums;i++)
        {      
            var ball = cc.instantiate(this.tgBlueBall);
            var tempNum = i+1;
            var oneNum = tempNum>9 ? tempNum.toString():"0"+tempNum.toString(); 
            ball.getComponent('Bet_toggle_ball').init({
                num: oneNum,
                miss: (this._isMiss == true)?i:""
            });
            var checkEventHandler = new cc.Component.EventHandler();
            checkEventHandler.target = this.node; 
            checkEventHandler.component = "superLottoPage"
            checkEventHandler.handler = "onSelBlueBallCallback";
            checkEventHandler.customEventData = {index:i,num:oneNum};//index 球下标 num 球数值 
            ball.getComponent(cc.Toggle).checkEvents.push(checkEventHandler);
            this.betContent.node.addChild(ball);
            this._twoBallGroups.push(ball);
            this._allBalls.push(ball);

                //事件监听
            //当手指在目标节点区域内离开屏幕时。
            ball.on(cc.Node.EventType.TOUCH_END, function (event) {
                this.spBigBlueBall.node.y = 10000;
            },this);

            //当手指在目标节点区域外离开屏幕时。
            ball.on(cc.Node.EventType.TOUCH_CANCEL, function (event) {
                this.spBigBlueBall.node.y = 10000;
            },this);

            //当手指触摸到屏幕时。
            ball.on(cc.Node.EventType.TOUCH_START, function (event) {
                var curTg = event.getCurrentTarget();
                var pos = curTg.getPosition();
                this.spBigBlueBall.node.getChildByName("labnum").getComponent(cc.Label).string = curTg.getComponent('Bet_toggle_ball').getNum();
                this.spBigBlueBall.node.x = pos.x;
                this.spBigBlueBall.node.y = pos.y-this._offsetY;
            },this);

            //当鼠标在目标节点在目标节点区域中移动时，不论是否按下。
            ball.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                var curTg = event.getCurrentTarget();
                var contentRect = curTg.getBoundingBoxToWorld();
                var touchLocation = event.getLocation();
                if(cc.rectContainsPoint(contentRect, touchLocation) == false){ 
                    this.spBigBlueBall.node.y = 10000;
                }  
            },this);

            //当手指在屏幕上目标节点区域内移动时。
            ball.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
                var curTg = event.getCurrentTarget();
                var contentRect = curTg.getBoundingBoxToWorld();
                var touchLocation = event.getLocation();
                if(cc.rectContainsPoint(contentRect, touchLocation) == false){
                    this.spBigBlueBall.node.y = 10000;
                }  
            },this);
        }

        this.onCreateCart();
    },

    //红球段选择
    onSelRedBallCallback:function(toggle, customEventData){
        var index = customEventData["index"];
        var num = customEventData["num"];
        if(this.isNorm == 0)
        {
            toggle.checkMark.spriteFrame = this.spYellowBall;  
            if(toggle.getComponent(cc.Toggle).isChecked)
            {
                if(this._selYellowBall.length >= 4)
                {
                    ComponentsUtils.showTips("胆码不能大于4个！");
                    toggle.getComponent(cc.Toggle).isChecked = false;
                    return;
                }

                toggle.getComponent("Bet_toggle_ball").setType(2);
                this._selYellowBall.push(num);
                this.updateTgSelect();
                this.checkBet();
                return;
            }
        }
        else if(this.isNorm == 1)
        {
            toggle.checkMark.spriteFrame = this.spRedBall;
            if(toggle.getComponent(cc.Toggle).isChecked)
            {
                toggle.getComponent("Bet_toggle_ball").setType(1);
                this._selRedBall.push(num);
                this.updateTgSelect();
                this.checkBet();
                return;
            }
        }
 
        var type = toggle.getComponent("Bet_toggle_ball").getType();
        if(type == 1)
        {
            Utils.removeByValue(this._selRedBall,num);
        }
        else if(type == 2)
        {
            Utils.removeByValue(this._selYellowBall,num);
        }
        toggle.getComponent("Bet_toggle_ball").setType(0);
        this.updateTgSelect();
        this.checkBet();

    },

    //蓝球段选择
    onSelBlueBallCallback:function(toggle, customEventData){
        var index = customEventData["index"];
        var num = customEventData["num"];
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            this._selBlueBall.push(num);
        }
        else
        {
            Utils.removeByValue(this._selBlueBall,num);
        }
        this.updateTgSelect();
        this.checkBet();
    },

    //胆拖设置
    onChangeBallCallback:function(toggle, customEventData){
       if(toggle.getComponent(cc.Toggle).isChecked)
       {
            this.isNorm = 0;
            ComponentsUtils.showTips("开始胆拖投注");
            this.labTopTitle.string = "胆拖投注";
       }
       else
       {
            this.isNorm = 1;
            this.labTopTitle.string = "普通投注";
       }
       this.updateTgSelect();
    },

    //机选
    randomSelectCallBack:function(){
        if(this.isNorm == 0 || this._selYellowBall.length>0 )
            return false; 
        this.clearCurBetRecord();
        var randomRedArray = Utils.getRandomArrayWithArray(this._oneBallGroups, 5);
        for(var i=0;i<randomRedArray.length;i++)
        {
            randomRedArray[i].getComponent(cc.Toggle).isChecked = true;
            var temp = randomRedArray[i].getComponent(cc.Toggle).checkEvents[0].customEventData;
            this.onSelRedBallCallback(randomRedArray[i].getComponent(cc.Toggle),temp);
        }   

        var randomBlueArray = Utils.getRandomArrayWithArray(this._twoBallGroups, 2);
        for(var i=0;i<randomBlueArray.length;i++)
        {
            randomBlueArray[i].getComponent(cc.Toggle).isChecked = true;
            var temp = randomBlueArray[i].getComponent(cc.Toggle).checkEvents[0].customEventData;
            this.onSelBlueBallCallback(randomBlueArray[i],temp);
        }
        return true;
    },

    //选择清空切换
    onSelectCallBack:function(toggle){
        if(this.isNorm == 0)
        {
            toggle.getComponent(cc.Toggle).isChecked = true;
            this.clearCurBetRecord();
        }
        else
        {
            if(toggle.getComponent(cc.Toggle).isChecked)
            {
                this.randomSelectCallBack();
            }
            else
            {
                this.clearCurBetRecord();
            }
        }
    },

    updateTgSelect:function(){
        if(this._selRedBall.length>0 || this._selYellowBall.length>0 || this._selBlueBall.length>0)
        {
            this.tgSelect.getComponent(cc.Toggle).isChecked = true;
        }
        else
        {
            if(this.isNorm == 1)
            {
                this.tgSelect.getComponent(cc.Toggle).isChecked = false;
            }
            else
            {
                this.tgSelect.getComponent(cc.Toggle).isChecked = true;
            }
        } 
    },

    //清空选择
    clearCurBetRecord:function(){
        for(var i=0;i<this._allBalls.length;i++)
        {
            this._allBalls[i].getComponent(cc.Toggle).isChecked = false;
        }

        this._changeBall.getComponent(cc.Toggle).isChecked = false;
        this.tgSelect.getComponent(cc.Toggle).isChecked = false;
        
        this.isNorm = 1;
        this._selRedBall.length = 0;
        this._selYellowBall.length = 0;
        this._selBlueBall.length = 0;
        this.setMutipleAmount("1");
        this.setBetNum(0);
        this.spBigYellowBall.node.y = 10000;
        this.spBigRedBall.node.y = 10000;
        this.spBigBlueBall.node.y = 10000;
        this.checkBet();
    },

     //计算
    checkBet:function(){
        var bet = 0;
        if(this._selYellowBall.length >0)
        {
             var bet1,bet2=0;
            var len0 = this._selRedBall.length;
            var len1 = this._selYellowBall.length;
            var len2 = this._selBlueBall.length;
            var bet1 = LotteryUtils.combination(len0,5-len1);
            var bet2 = LotteryUtils.combination(len2,2);
            bet = bet1 * bet2;
            if(bet == 1)//胆拖必须2注起投
                bet = 0;
            if(bet != 0 && len0 + len1 == 5)
            {
                bet = 0;
            }  
        }
        else
        {
            var bet1,bet2=0;
            var bet1 = LotteryUtils.combination(this._selRedBall.length,5);
            var bet2 = LotteryUtils.combination(this._selBlueBall.length,2);
            bet = bet1 * bet2;
        }

        var muiple = this.getMutipleAmount();
        this.setShowAmount(muiple,bet);
    },

   //设置金额
    setMoney:function(mon){
        this._curMoney = mon;
    },

    //得到金额
    getMoney:function(){
        return this._curMoney;
    },

    //设置注数
    setBetNum:function(num){
        this._curBetNum = num;
    },

    //得到注数
    getBetNum:function(){
        return this._curBetNum;
    },

    //设置倍数
    setMutipleAmount:function(mutiple){
         this.edMutipleNum.string = mutiple;
         this.checkBet();
    },

    //获取当前倍数
    getMutipleAmount:function(){
        var amount = parseInt(this.edMutipleNum.string);
        if(isNaN(amount)){ 
            return 1;
        }else{
            return amount;
        }        
    },

    //加倍
    onAddBtn:function(){
        var amount = this.getMutipleAmount();
        if(!isNaN(amount) && amount<999){
            amount = amount+1;
            this.setMutipleAmount(amount.toString());
        }
    },

    //减倍
    onReduceBtn:function(){
        var amount = this.getMutipleAmount();
        if(!isNaN(amount)){
            if(amount>0){
                amount = amount-1;
                if(amount<=0)
                {
                    ComponentsUtils.showTips("倍数不能小于1！");
                    amount = 1;
                }
                this.setMutipleAmount(amount.toString());
            }
        }
    },
    
    //手动倍数
    onManualEditBoxTextChanged:function(editbox) {
        if(!Utils.isInt(editbox.string))
        {
            ComponentsUtils.showTips("输入格式错误！");
            editbox.string = "1";
        }
        if(editbox.string == null || editbox.string == "")
        {
            editbox.string = "1";
            ComponentsUtils.showTips("倍数不能为空！");
        }

        var amount = parseInt(editbox.string);
        if(isNaN(amount) || amount > 999 || amount<=0 ){
            editbox.string = "1";
        }
  
        this.checkBet();
    },

    //显示投注信息
    setShowAmount:function(mut,bet){
        var money = mut*bet*this.BASEMONEY;
        this.setMoney(money);
        this.setBetNum(bet);
        this.labSelectShow.string = "共"+bet+"注"+money+"元";
    },

    //投注信息组合
    dataToJson:function(){
        var idx = this._isAdd ==true ? 1:0;
        var PlayCode = parseInt(this._lotteryID + this._rules[idx]); 
        var nums = "";
        var num = "";
        var isnr = this._selYellowBall.length>0?0:1;
        if(isnr == 1)
        {
            var ball1 = this._selRedBall;
            Utils.sortNum(ball1);
            num = "";
            for(var i = 0;i<ball1.length;i++)
            {
                if(num != "")
                {
                    num+=",";
                }  
                num += ball1[i].toString();
            }
            nums = num;
            var ball2 = this._selBlueBall;
            Utils.sortNum(ball2);
            num = "";
            for(var i = 0;i<ball2.length;i++)
            {
                if(num != "")
                {
                    num+=",";
                }  
                num += ball2[i].toString();
            }
            nums = nums+"|"+num;
        }
        else
        {
            var ball1 = this._selYellowBall;
            Utils.sortNum(ball1);
            num = "";
            for(var i = 0;i<ball1.length;i++)
            {
                if(num != "")
                {
                    num+=",";
                }  
                num += ball1[i].toString();
            }
            nums = num;

            var ball2 = this._selRedBall;
            Utils.sortNum(ball2);
            num = "";
            for(var i = 0;i<ball2.length;i++)
            {
                if(num != "")
                {
                    num+=",";
                }  
                num += ball2[i].toString();
            }
            nums = nums+"#"+num;

            var ball3 = this._selBlueBall;
            Utils.sortNum(ball3);
            num = "";
            for(var i = 0;i<ball3.length;i++)
            {
                if(num != "")
                {
                    num+=",";
                }  
                num += ball3[i];
            }
            nums = nums+"|"+num;
        } 

      var numArrays = {
            "PlayCode":PlayCode,
            "Multiple":this.getMutipleAmount(),
            "Bet":this.getBetNum(),
            "isNorm":isnr,
            "Number":nums
        };
        return numArrays;
    },

   //前往号码栏
    onGotoBetCart:function(){ 
        if(this.addBetNumber())
        {
            this._betCart.active = true;
            this.clearCurBetRecord();
        }
        else
        {
            if(!this._betCart.getComponent("BetCartPage").isHasBetNumbers())
            {
                ComponentsUtils.showTips("请先选择投注!");
            }
            else
            {
                this._betCart.active = true;
                this.clearCurBetRecord();
            }
        }
    },

    //添加一注
    addBetNumber:function(){
         if(this.getMoney() <= 0)
            return false;
        var data = {
            lotteryId:this._lotteryID,//彩种id
            dataBase:this.dataToJson(),//投注信息
            otherBase:"",//追加
            money:this.getMoney(),
            isAdd:this._isAdd
        }
        this._betCart.getComponent("BetCartPage").addBetNumber(data);
        return true;
    },

 //关闭界面
    onClose:function(){
        if(this._betCart)
        {
             this._betCart.getComponent("Page").backAndRemove();
             this._betCart = null;
        }     
        window.Notification.emit("BET_ONCLOSE","");
    },

    //机选
    randomAnySelect:function(betnum){
        for(var j=0;j<betnum;j++)
        {
            var randomRedArray = Utils.getRandomArrayWithArray(this._oneBallGroups,5);
            for(var i=0;i<randomRedArray.length;i++)
            {
                randomRedArray[i].getComponent(cc.Toggle).isChecked = true;
                var temp = randomRedArray[i].getComponent(cc.Toggle).checkEvents[0].customEventData;
                this.onSelRedBallCallback(randomRedArray[i].getComponent(cc.Toggle),temp);
            }   

            var randomBlueArray = Utils.getRandomArrayWithArray(this._twoBallGroups, 2);
            for(var i=0;i<randomBlueArray.length;i++)
            {
                randomBlueArray[i].getComponent(cc.Toggle).isChecked = true;
                var temp = randomBlueArray[i].getComponent(cc.Toggle).checkEvents[0].customEventData;
                this.onSelBlueBallCallback(randomBlueArray[i].getComponent(cc.Toggle),temp);
            }   
            this.addBetNumber();
            this.clearCurBetRecord();
        }
    },

    onMiss:function(toggle){
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            this._isMiss = true;
            this._betManage.showMissArry(this._toggleLists,this._lotteryID,this._lotteryID + this._rules[0].toString());
        }    
        else
        {
            this._isMiss = false;
            this._betManage.setMissArry(false,this._toggleLists,"");
        }
        this.changeBetPanle(this._isMiss);
    },

    changeBetPanle:function(state){
        if(state)//遗漏
        {
            for(var i=0;i<this._toggleLists.length;i++)
            {
                for(var j=0;j<this._toggleLists[i].length;j++)
                {
                    this._toggleLists[i][j].height = 180;
                }
            }
            this.ndCentent.height = 1805;
            this.ndBetBg.height = 1805;
            this.ndBallCentent = 1350;
            this.ndBallCentent1 = 1350;
            this.labTime.node.active = true;
        }
        else//还原
        {
            for(var i=0;i<this._toggleLists.length;i++)
            {
                for(var j=0;j<this._toggleLists[i].length;j++)
                {
                    this._toggleLists[i][j].height = 159;
                }
            }
            this.ndCentent.height = 1585;
            this.ndBetBg.height = 1585;
            this.ndBallCentent = 1210;
            this.ndBallCentent1 = 1210;
            this.labTime.node.active = false;
        }
    },

    update:function(dt){
        var issueStr = TrentChart.getCurIssue();
        if(issueStr != "")
        {
            var currentTimeStamp = Date.parse(new Date())/1000;
            var time = 0;
            var endtime = TrentChart.getEndTime();
            if(endtime == "91")
            {
                this.labTime.string = "本期已封盘";
                return;
            } 
            else if(endtime == "90")
            {
                this.labTime.string = "预售中"; 
                return;
            }
            var leftTimeStamp = endtime - currentTimeStamp;
            if(leftTimeStamp >= 0){
                time = endtime - currentTimeStamp;
            }
            else
            {
                this.labTime.string = "--:--:--";
                return;
            }
            var h = parseInt(time/(60*60)).toString();
            var hStr = (parseInt(h)%24).toString();
            var dStr = parseInt( parseInt(h)/24).toString();
            hStr = hStr.length==1?("0"+hStr):hStr;
            var m = (parseInt((time-h*60*60)/60)).toString();
            m = m.length==1?("0"+m):m;
            var s = (parseInt(time-h*60*60-m*60)).toString();
            s = s.length==1?("0"+s):s;
            var timeStr = "";
            if(parseInt(dStr)>0){
                timeStr = dStr+"天 "+ hStr+":"+m+":"+s;
            }else{
                timeStr = hStr+":"+m+":"+ s;
            }
            this.labTime.string =  timeStr;
        }
        else
            this.labTime.string = "";
    },

});
