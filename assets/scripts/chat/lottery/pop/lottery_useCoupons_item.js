/**
 * !#zh 彩券组件
 * @information 彩券可使用钱数，使用范围，使用有效期
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labDec:cc.Label,
        labMoney:cc.Label,
        labTime:cc.Label,
        labRange:cc.Label,
        tgSel:cc.Toggle,
        _data:null, //数据信息
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.labDec.string = this._data.dec;
            this.labMoney.string = this._data.money;
            this.labTime.string = this._data.time;
            this.labRange.string = this._data.range;
            this.tgSel.getComponent(cc.Toggle).isChecked = this._data.isCheck;
        }
    },


    /** 
    * 接收彩券数据
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

});
